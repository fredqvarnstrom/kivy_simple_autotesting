import pytest
from tools import simulate

# import simulator
from simulator_fixture import simulator


@pytest.mark.parametrize("params", [{}])
@simulate
def test_example(sim):

    # widgets are selected with xpath
    sim.assert_count("//RootWidget//Button", 3)

    # deep tree goes reversed through the tree
    sim.assert_text("//RootWidget//Button[1]", "First")
    sim.assert_text("//RootWidget//Button[2]", "Second")
    sim.assert_text("//RootWidget//Button[3]", "Third")

    sim.assert_text("//RootWidget//StatusLabel", "Status")

    sim.tap("//RootWidget//Button[1]")
    sim.assert_text("//RootWidget//StatusLabel", "First pressed")

    sim.tap("//RootWidget//Button[2]")
    sim.assert_text("//RootWidget//StatusLabel", "Second pressed")

    sim.assert_disabled("//RootWidget//Button[3]")
